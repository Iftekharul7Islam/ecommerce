<!doctype html>
<html lang="en">
<?php include_once('elements/head.php');?>
<body>

<?php include_once('elements/header.php');?>


<main role="main">

    <?php include_once('elements/slider.php');?>



    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

        <!-- Three columns of text below the carousel -->
        <div class="row">
            <?php include('elements/home_block_product.php');?>
            <?php include('elements/home_block_product.php');?>
            <?php include('elements/home_block_product.php');?>

        </div>

        <!-- START THE FEATURETTES -->

        <hr class="featurette-divider">

        <?php include_once('elements/home_feature_right.php');?>

        <?php include_once('elements/home_feature_left.php');?>


        <!-- /END THE FEATURETTES -->

    </div>

    <?php include_once('elements/footer.php');?>

</main>

<?php include_once('elements/scripts.php');?>

</body>
</html>