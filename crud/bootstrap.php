<?php
define('WEBROOT', 'http://localhost/crud/');
define('ADMIN', 'http://localhost/crud/admin/');
define('VIEW', 'http://localhost/crud/admin/views/');

define('LIB', 'http://localhost/crud/lib/');
define('CSS', 'http://localhost/crud/lib/css/');
define('JS', 'http://localhost/crud/lib/js/');
define('IMG', 'http://localhost/crud/lib/img/');
define('UPLOADS', 'http://localhost/crud/uploads/');
define('DOCROOT', $_SERVER['DOCUMENT_ROOT'].'/crud/');

//connect to database
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "phpcrud";

//PDO - connection string
$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
// set the PDO error mode to exception
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//importing layout
ob_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/admin/views/layouts/admin.php');
$layout = ob_get_contents();
ob_end_clean();












