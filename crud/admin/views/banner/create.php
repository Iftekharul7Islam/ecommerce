<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');
?>

<?php
ob_start();
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-12 px-4">
    
            <form id="banners-entry-form" method="post" action="store.php" role="form">

                <div class="messages"></div>
                <h1>ADD NEW BANNER</h1>
                <div class="controls">
                    <div class="row">

                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="title">Banner Title</label>
                                <input id="title"
                                       value=""
                                       type="text"
                                       name="title"
                                       autofocus="autofocus"
                                       placeholder="e.g. Buy now"
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="picture">Banner Picture</label>
                                <input id="picture"
                                       value=""
                                       type="text"
                                       name="picture"
                                       placeholder="e.g. link of a picture"
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="promotional_msg ">Promotional Message</label>
                                <input id="promotional_msg"
                                       value=""
                                       type="text"
                                       name="promotional_msg"
                                       placeholder="e.g. promotional message"
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="html_banner">HTML Banner </label>
                                <input id="html_banner "
                                       value=""
                                       type="text"
                                       name="html_banner "
                                       placeholder="e.g. html banner link"
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                    </div>

<button type="submit" class="btn btn-success">
    Send & Save Banner
</button>

                </div>

            </form>
        </main>

<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>