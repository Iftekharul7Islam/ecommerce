<?php
//collect the data
$title = $_POST['title'];
$picture = $_POST['picture'];
//$link = $_POST['link '];
$promotional_msg = $_POST['promotional_msg'];
$html_banner = $_POST['html_banner'];

include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//prepare the insert query
$query = "INSERT INTO `banners` ( 
`title`, 
`picture`, 
`link`, 
`promotional_msg`, 
`html_banner`) 
VALUES ( :title, :picture, NULL, :promotional_msg, :html_banner)";

$sth = $conn->prepare($query);

$sth->bindParam(':title', $title);
$sth->bindParam(':picture', $picture);
$sth->bindParam(':promotional_msg', $promotional_msg);
$sth->bindParam(':html_banner', $html_banner);

$sth->execute();

//redirect to index page
header('location:index.php');