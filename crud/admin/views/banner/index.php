<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//selection query
$query = "SELECT * FROM banners";
$sth = $conn->prepare($query);
$sth->execute();

$banners = $sth->fetchAll(PDO::FETCH_ASSOC);

?>

<?php
ob_start();
?>
    <main role="main" class="col-md-9 ml-sm-auto col-lg-12 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Banner</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <button type="button" class="btn btn-sm btn-outline-secondary">
                        <span data-feather="calendar"></span>
                        <a href="<?=VIEW;?>banner/create.php" style="color: black">Add New</a>
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 ftco-animate">
                    <div class="cart-list">
                        <table class="table">
                            <thead class="thead-primary">
                            <tr class="text-center">
                                <th>&nbsp;</th>
                                <th>Picture</th>
                                <th>Title</th>
                                <th>Message</th>
                                <th>Banner</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if($banners){
                                foreach ($banners as $banner){
                                    ?>
                                    <tr class="text-center">
                                        <td class="banner-sl"><a href="#"><span class="ion-ios-close"></span></a></td>

                                        <td class="image-prod"><div class="img"><img src="<?php echo $banner['picture'];?>" width="140px" height="120px"></div></td>

                                        <td class="banner-name">
                                            <h3><a href="show.php?id=<?php echo $banner['id'];?>"><?php echo $banner['title'];?></a></h3>
                                        </td>
                                        <td class="banner-msg">
                                            <p><?php echo $banner['promotional_msg'];?></p>
                                        </td>
                                        <td class="html-banner">
                                            <div class="img"><img src="<?php echo $banner['html_banner'];?>" width="140px" height="180px"></div>
                                        </td>
                                        <td><a href="edit.php?id=<?php echo $banner['id'];?>">Edit</a> |
                                            <a href="delete.php?id=<?php echo $banner['id'];?>">Delete</a></td>
                                    </tr>
                                <?php } }else{

                            ?>
                                <tr class="text-center">
                                    <td colspan="6">there is no banner available <a href="create.php">click here</a> to add a banner</td>
                            </tr>
                           <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </main>

<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>