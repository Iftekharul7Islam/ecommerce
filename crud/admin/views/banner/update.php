<?php
//collect the data
$id = $_POST['id'];
$title = $_POST['title'];
$picture = $_POST['picture'];
$promotional_msg = $_POST['promotional_msg'];
$html_banner = $_POST['html_banner'];

include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//prepare the update query
$query = "UPDATE `banners` SET 
`title` = :title, 
`picture` = :picture, 
`promotional_msg` = :promotional_msg,
`html_banner` = :html_banner
WHERE `banners`.`id` = :id";

$sth = $conn->prepare($query);

$sth->bindParam(':id', $id);
$sth->bindParam(':title', $title);
$sth->bindParam(':picture', $picture);
$sth->bindParam(':promotional_msg', $promotional_msg);
$sth->bindParam(':html_banner', $html_banner);

$sth->execute();

//redirect to index page
header('location:index.php');