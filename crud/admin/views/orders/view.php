<?php
//collect the id
$id = $_GET['id'];

include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//selection query
$query = "SELECT * FROM products WHERE id = :id";

$sth = $conn->prepare($query);
$sth->bindParam(':id', $id);
$sth->execute();

$order = $sth->fetch(PDO::FETCH_ASSOC);

?>
<?php
ob_start();
?>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-12 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Order view</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <button type="button" class="btn btn-sm btn-outline-secondary">
                    <span data-feather="calendar"></span>
                    <a href="<?=VIEW;?>order/index.php" style="color: black">Go to List</a>
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <section>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm col-md-6 col-lg-3">
                                <div class="product">

                                    <div class="text py-3 px-3">
                                        <h3><a href="#"><?php echo $order['product_id'];?></a></h3>
                                        <p><?php echo $order['qty'];?></p>
                                </div>
                </section>
            </div>
        </div>
    </main>

<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>