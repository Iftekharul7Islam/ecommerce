<?php
//collect the id
$id = $_GET['id'];

include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//selection query
$query = "SELECT * FROM orders WHERE id = :id";

$sth = $conn->prepare($query);
$sth->bindParam(':id', $id);
$sth->execute();

$order = $sth->fetch(PDO::FETCH_ASSOC);

?>


<?php
ob_start();
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-12 px-4">

    <form id="products-entry-form" method="post" action="update.php" role="form">

        <div class="messages"></div>
        <h1>Edit Products</h1>
        <div class="controls">
            <div class="row">

                <input id="id"
                       value="<?php echo $order['id'];?>"
                       type="hidden"
                       name="id"
                       class="form-control">
                <div class="col-lg-8">
                    <div class="form-group">
                        <label for="product_id">Product_id</label>
                        <input id="product_id"
                               value="<?php echo $order['product_id'];?>"
                               type="text"
                               name="product_id"
                               autofocus="autofocus"
                               placeholder="e.g. Bashundhara Tissue"
                               class="form-control">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="form-group">
                        <label for="qty">Quantity</label>
                        <input id="qty"
                               value="<?php echo $order['qty'];?>"
                               type="text"
                               name="qty"
                               placeholder="e.g. Bashundhara"
                               class="form-control">

                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-success">
                Send & Save Product
            </button>

        </div>

    </form>
</main>

<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>
