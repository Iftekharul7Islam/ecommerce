<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');
?>

<?php
ob_start();
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-12 px-4">

    <form id="product-entry-form" method="post" action="store.php" role="form">

        <div class="messages"></div>
        <h1>ADD NEW Order</h1>
        <div class="controls">
            <div class="row">
                <div class="col-lg-8">
                    <div class="form-group">
                        <label for="product_id">Product_id</label>
                        <input id="product_id"
                               value=""
                               type="text"
                               name="product_id"
                               autofocus="autofocus"
                               placeholder="e.g. Bashundhara Tissue"
                               class="form-control">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="form-group">
                        <label for="qty">Quantity</label>
                        <input id="qty"
                               value=""
                               type="text"
                               name="qty"
                               placeholder="e.g. Bashundhara"
                               class="form-control">

                    </div>
                </div>

                </div>


            <button type="submit" class="btn btn-success">
                Send & Save </a>
            </button>
        </div>


    </form>
</main>

<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>
