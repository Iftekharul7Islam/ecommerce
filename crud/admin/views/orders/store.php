<?php
$product_id = $_POST['product_id'];
$quantity = $_POST['qty'];
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');
$query = "INSERT INTO `orders`
(
`product_id`,
`qty`)
VALUES
(
:product_id)";
$sth = $conn->prepare($query);

$sth->bindParam(':product_id', $product_id);
$sth->bindParam(':qty', $qty);
$result = $sth->execute();


//redirect to index page
header('location:index.php');