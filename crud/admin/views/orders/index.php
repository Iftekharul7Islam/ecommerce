<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//selection query
$query = "SELECT * FROM orders";
$sth = $conn->prepare($query);
$sth->execute();

$orders = $sth->fetchAll(PDO::FETCH_ASSOC);


?>
<?php
ob_start();
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-12 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Order</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <button type="button" class="btn btn-sm btn-outline-secondary">
                <span data-feather="calendar"></span>
                <a href="<?=VIEW;?>order/add.php" style="color: black">Add New</a>
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ftco-animate">
            <div class="cart-list">
                <table class="table">
                    <thead class="thead-primary">
                    <tr class="text-center">
                        <th>&nbsp;</th>
                        <th>Order_id</th>
                        <th>Quantity</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($orders){
                        foreach ($orders as $order){
                            ?>
                            <tr class="text-center">
                                <td class="product-sl"><a href="#"><span class="ion-ios-close"></span></a></td>


                                <td class="product-name">
                                    <h3><a href="view.php?id=<?php echo $order['id'];?>"><?php echo $order['product_id'];?></a></h3>

                                </td>
                                <td class="product-quantity">
                                    <h3><?php echo $order['qty'];?></h3>

                                </td>
                                <td><a href="<?=VIEW;?>order/edit.php?id=<?php echo $order['id'];?>">Edit</a> |
                                    <a href="<?=VIEW;?>order/delete.php?id=<?php echo $order['id'];?>">Delete</a></td>
                            </tr>
                        <?php } }else{

                        ?>
                        <tr class="text-center">
                            <td colspan="6">there is no product available<a href="add.php">click here</a>to add a product</td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>
