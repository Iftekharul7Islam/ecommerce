<?php
$id = $_POST['id'];
$product_id = $_POST['product_id'];
$quantity = $_POST['qty'];
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');
$query = "UPDATE `products` SET 
`product_id` = :product_id,
 `qty`=:qty
WHERE 
`orders`.`id` = :id";
$sth = $conn->prepare($query);

$sth->bindParam(':product_id', $product_id);
$sth->bindParam(':qty', $qty);
$result = $sth->execute();


//redirect to index page
header('location:index.php');