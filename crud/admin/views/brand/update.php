<?php
//collect the data
$id = $_POST['id'];
$title = $_POST['title'];
$logo = $_POST['logo'];

include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//prepare the insert query
$query = "UPDATE `brands` SET 
`title` = :title, 
`logo` = :logo
WHERE `brands`.`id` = :id";

$sth = $conn->prepare($query);

$sth->bindParam(':id', $id);
$sth->bindParam(':title', $title);
$sth->bindParam(':logo', $logo);

$sth->execute();

//redirect to index page
header('location:index.php');