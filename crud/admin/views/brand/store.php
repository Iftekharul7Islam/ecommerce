<?php
//collect the data
$title = $_POST['title'];
$logo = $_POST['logo'];

include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//prepare the insert query
$query = "INSERT INTO `brands` (
`title`, 
`logo`, 
`link`) 
VALUES ( :title, :logo, NULL)";

$sth = $conn->prepare($query);

$sth->bindParam(':title', $title);
$sth->bindParam(':logo', $logo);

$sth->execute();

//redirect to index page
header('location:index.php');