<?php
//collect the id
$id = $_GET['id'];

include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//selection query
$query = "SELECT * FROM brands WHERE id = :id";

$sth = $conn->prepare($query);
$sth->bindParam(':id', $id);
$sth->execute();

$brand = $sth->fetch(PDO::FETCH_ASSOC);

?>

<?php
ob_start();
?>
    <main role="main" class="col-md-9 ml-sm-auto col-lg-12 px-4">
        
            <form id="brand-entry-form" method="post" action="update.php" role="form">

                <div class="messages"></div>
                <h1>Edit Brand</h1>
                <div class="controls">
                    <div class="row">
                        
                        <input id="id"
                               value="<?php echo $brand['id'];?>"
                               type="hidden"
                               name="id"
                               class="form-control">
                        
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="title">Brand Name</label>
                                <input id="title"
                                       value="<?php echo $brand['title'];?>"
                                       type="text"
                                       name="title"
                                       autofocus="autofocus"
                                       placeholder="e.g. Bashundhara"
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="logo">Brand Logo</label>
                                <input id="logo"
                                       value="<?php echo $brand['logo'];?>"
                                       type="text"
                                       name="logo"
                                       placeholder="e.g. link of a logo"
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        
                    </div>

<button type="submit" class="btn btn-success">
    Send & Save Brand
</button>

                </div>

            </form>
        </main>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>