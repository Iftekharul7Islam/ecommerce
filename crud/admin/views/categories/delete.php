<?php
$id=$_GET['id'];
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

$query="DELETE FROM `categories` WHERE `categories`.`id` = :id;";
$sth = $conn->prepare($query);
$sth->bindparam(':id',$id);
$result=$sth->execute();
header("location:index.php");