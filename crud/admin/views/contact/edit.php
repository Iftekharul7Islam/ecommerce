<?php
//.....connect to database
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//selection query
$query = "SELECT * FROM contacts WHERE id=".$_GET['id'];
$sth = $conn->prepare($query);
$sth->execute();

/* Fetch all of the remaining rows in the result set */
$contact= $sth->fetch(PDO::FETCH_ASSOC);
//print_r($testimonial);
?>


        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">



            <form id="contact-form" method="post" action="update.php" role="form">

                <div class="messages"></div>
                <h1>Edit</h1>
                <div class="controls">
                    <div class="row">
                      <div class="col-lg-12">
                            <div class="form-group">
                               <!-- <label for="id">ID</label>-->
                                <input id="id"  value="<?php echo $contact['id'] ?>" type="hidden" name="id" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input id="name"  value="<?php echo $contact['name'] ?>" type="text" name="name" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input id="email"  value="<?php echo $contact['email'] ?>" type="text" name="email" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="subject">Subject</label>
                                <input id="subject"  value="<?php echo $contact['subject'] ?>" type="text" name="subject" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <!--<div class="col-lg-6">
                            <div class="form-group">
                                <label for="comment">Comment</label>
                                <input id="comment"  value="" type="text" name="comment" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                     <div class="col-lg-6">
                            <div class="form-group">
                                <label for="status ">Status</label>
                                <input id="status"  value="" type="text" name="status" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>-->
                     <!--   <div class="col-lg-6">
                            <div class="form-group">
                                <label for="test_is_draft ">test_is_draft </label>
                                <input id="test_is_draft "  value="" type="text" name="test_is_draft " class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="test_soft_delete ">test_soft_delete </label>
                                <input id="test_soft_delete "  value="" type="text" name="test_soft_delete " class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="test_created_at ">test_created_at </label>
                                <input id="test_created_at "  value="" type="text" name="test_created_at " class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="test_modified_at">test_modified_at</label>
                                <input id="test_modified_at"  value="" type="text" name="test_modified_at" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>-->
                    </div>

                    <button type="submit" class="btn btn-success">Send & Save Contact</button>
                    <!--<input type="submit" class="btn btn-success btn-send" value="Send & Save message">-->


                </div>

            </form>
        </main>
            <?php
            $pagecontent = ob_get_contents();
            ob_end_clean();
            echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
            ?>
