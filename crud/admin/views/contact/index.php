<?php
//.....connect to database
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');
//selection query
$query = "SELECT * FROM contacts";
$sth = $conn->prepare($query);
$sth->execute();

/* Fetch all of the remaining rows in the result set */
$contacts = $sth->fetchAll(PDO::FETCH_ASSOC);
//print_r($testimonials);
?>

<?php
ob_start();
?>




        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
                <h1>Contact</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <button type="button" class="btn btn-sm btn-outline-secondary">
                        <span data-feather="calendar"></span>
                        <a href="add.php" style="color: black">Add New</a>
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 ftco-animate">
                    <div class="cart-list">
                        <table class="table">
                            <thead class="thead-primary">
                            <tr class="text-center">
                                <th></th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if($contacts){
                            foreach($contacts as $contact)
                            {
                                ?>
                                <tr class="text-center">
                                    <td class="contact-sl"><a href="#"><span class="ion-ios-close"></span></a></td>

                                    <td class="contact-name">
                                        <h3><a href="view.php?id=<?php echo $contact['id'];?>"><?php echo $contact['name']; ?></a></h3>

                                    </td>

                                    <td class="contact-email">
                                        <h3><?php echo $contact['email']; ?></h3>
                                    </td>

                                    <td><a href="<?=VIEW;?>contact/edit.php?id=<?php echo $contact['id'];?>">Edit</a>
                                        | <a href="<?=VIEW;?>contact/delete.php?id=<?php echo $contact['id'];?>">Delete</a></td>
                                </tr>
                            <?php }} else{
                                ?>
                                <tr class="text-center"><td colspan="4">There is no contact available</td></tr>
                            <?php
                            }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </main>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>
