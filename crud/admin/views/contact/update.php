<?php
//collect the data
$id=$_POST['id'];
$name=$_POST['name'];
$email=$_POST['email'];
$subject=$_POST['subject'];

//.....connect to database
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');
//connecting to database

//prepare the insert query
$query = "UPDATE `contacts` SET `name` = :name, `email` = :email,
 `subject` = :subject WHERE `contacts`.`id` = :id;";

$sth = $conn->prepare($query);
$sth->bindParam(':id',$id);
$sth->bindParam(':name',$name);
$sth->bindParam(':email',$email);
$sth->bindParam(':subject',$subject);
$result=$sth->execute();
//print_r($result);
$sth=null;
header("location:index.php");