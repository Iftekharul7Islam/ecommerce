<?php
//collect the data
$name=$_POST['name'];
$email=$_POST['email'];
$subject=$_POST['subject'];

//.....connect to database
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');


//prepare the insert query
$query = "INSERT INTO `contacts` ( `name`, `email`, `subject`, `comment`,
 `status`, `soft_delete`, `date`)
  VALUES ( :name, :email , :subject, NULL, NULL, NULL, NULL);";

$sth = $conn->prepare($query);
$sth->bindParam(':name',$name);
$sth->bindParam(':email',$email);
$sth->bindParam(':subject',$subject);
$result=$sth->execute();
//print_r($result);
$sth=null;
header("location:index.php");