<?php
$id=$_POST['id'];
$email=$_POST['email'];
$is_subscribed=$_POST['is_subscribed'];
$created_at=$_POST['created_at'];
$modified_at=$_POST['modified_at'];
$reason_text=$_POST['reason_text'];
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');
$query="UPDATE `subscribers` SET `email` = :email,
 `is_subscribed` = :is_subscribed, 
 `created_at` = :created_at,
  `modified_at` = :modified_at, 
  `reason_text` = :reason_text WHERE `subscribers`.`id` = :id;";
$sth = $conn->prepare($query);
$sth->bindparam(':id',$id);
$sth->bindparam(':email',$email);
$sth->bindparam(':is_subscribed',$is_subscribed);
$sth->bindparam(':created_at',$created_at);
$sth->bindparam(':modified_at',$modified_at);
$sth->bindparam(':reason_text',$reason_text);

$result=$sth->execute();
header("location:index.php");