<?php
$email=$_POST['email'];
$is_subscribed=$_POST['is_subscribed'];
$created_at=$_POST['created_at'];
$modified_at=$_POST['modified_at'];
$reason_text=$_POST['reason_text'];
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');
$query="INSERT INTO `subscribers` (
`email`, 
`is_subscribed`, 
`created_at`,
 `modified_at`,
  `reason_text`) VALUES (:email, 
  :is_subscribed,
   :created_at,
 :modified_at,
  :reason_text);";
$sth = $conn->prepare($query);
$sth->bindparam(':email',$email);
$sth->bindparam(':is_subscribed',$is_subscribed);
$sth->bindparam(':created_at',$created_at);
$sth->bindparam(':modified_at',$modified_at);
$sth->bindparam(':reason_text',$reason_text);
$result=$sth->execute();
print_r($result);
header("location:index.php");