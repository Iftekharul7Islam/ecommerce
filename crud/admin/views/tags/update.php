<?php
$id=$_POST['id'];
$title=$_POST['title'];
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

$query="UPDATE `tags` 
SET `title` = :title
WHERE `tags`.`id` = :id";
$sth = $conn->prepare($query);
$sth->bindparam(':id',$id);
$sth->bindparam(':title',$title);
$result=$sth->execute();
header("location:index.php");