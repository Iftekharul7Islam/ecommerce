<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');
?>

<?php
ob_start();
?>
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">



            <form id="contact-form" method="post" action="store.php" role="form">
                <div class="messages"></div>
                <h1>ADD NEW Popular_tag</h1>
                <div class="controls">
                    <div class="row">

                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input id="name"
                                       value=""
                                       type="text"
                                       name="name"
                                       placeholder="e.g.samsung" class="form-control"
                                       autofocus="autofocus";>

                                <div class="help-block text-muted">Enter  Popular_tag</div>
                            </div>
                        </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="link">link</label>
                        <input id="link"
                               value=""
                               type="text"
                               name="link"
                               placeholder="" class="form-control"
                               autofocus="autofocus";>


                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="ia">Is_active</label>
                        <input id="ia"
                               value=""
                               type="text"
                               name="ia"
                               placeholder="" class="form-control"
                               autofocus="autofocus";>


                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="id">Is_draft</label>
                        <input id="id"
                               value=""
                               type="text"
                               name="ia"
                               placeholder="" class="form-control"
                               autofocus="autofocus";>


                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="ca">created_at:</label>
                        <input id="ca"
                               value=""
                               type="datetime"
                               name="ca"
                               placeholder="" class="form-control"
                               autofocus="autofocus";>


                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="md">modified_at:</label>
                        <input id="md"
                               value=""
                               type="datetime"
                               name="ca"
                               placeholder="" class="form-control"
                               autofocus="autofocus";>


                    </div>
                </div>

                    </div>
                </div>


                <button type="submit" class="btn btn-success">send & Save</a></button>


    </form>
    </main>
        <?php
        $pagecontent = ob_get_contents();
        ob_end_clean();
        echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
        ?>