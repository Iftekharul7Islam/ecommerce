<?php
$id=$_POST['id'];
$name=$_POST['name'];
$link=$_POST['link'];
$is_active=$_POST['is_active'];
$is_draft=$_POST['is_draft'];
$soft_delete=$_POST['soft_delete'];
$created_at=$_POST['created_at'];
$modified_at=$_POST['modified_at'];

include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');
$query="UPDATE `popular_tag`
SET 
 `name` = :name,
  `link` =:link,
   `is_active` = :is_active, 
   `soft_delete` = :soft_delete, 
   `is_draft` = :is_draft, 
   `created_at` = :created_at, 
   `modified_at` = :modified_at 
   WHERE `popular_tag`.`id` = :id;
";
$sth = $conn->prepare($query);
$sth->bindparam(':name',$name);
$sth->bindparam(':link',$link);
$sth->bindparam(':is_active',$is_active);
$sth->bindparam(':soft_delete',$soft_delete);
$sth->bindparam(':is_draft',$is_draft);
$sth->bindparam(':created_at',$created_at);
$sth->bindparam(':modified_at',$modified_at);

$result=$sth->execute();

header("location:index.php");