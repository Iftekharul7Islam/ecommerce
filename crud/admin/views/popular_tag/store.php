<?php
$name=$_POST['name'];
$link=$_POST['link'];
$is_active=$_POST['is_active'];
$is_draft=$_POST['is_draft'];
$soft_delete=$_POST['soft_delete'];
$created_at=$_POST['created_at'];
$modified_at=$_POST['modified_at'];

include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

$query="INSERT INTO `popular_tag` ( `name`, 
`link`,
 `is_active`,
 `soft_delete`,
 `is_draft`, 
 `created_at`, 
 `modified_at`) VALUES ( :name, :link, :is_active, :soft_delete, :is_draft, :created_at, :modified_at)";

$sth = $conn->prepare($query);
$sth->bindparam(':name',$name);
$sth->bindparam(':link',$link);
$sth->bindparam(':is_active',$is_active);
$sth->bindparam(':soft_delete',$soft_delete);
$sth->bindparam(':is_draft',$is_draft);

$sth->bindparam(':created_at',$created_at);
$sth->bindparam(':modified_at',$modified_at);

$result=$sth->execute();

header("location:index.php");