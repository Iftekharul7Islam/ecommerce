<?php
//connect to database
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');
$query="select * from popular_tag";
$sth = $conn->prepare($query);
$sth->execute();
$pts=$sth->fetchAll(PDO::FETCH_ASSOC);
//print_r($result);
?>
<?php
ob_start();
?>


        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Popular_tag</h1>
                <button type="button" class="btn btn-sm btn-outline-secondary">
                    <span data-feather="calendar"></span>
                    <a href="add.php" style="color: black">Add New</a>
                </button>

            </div>
            <div class="row">
                <div class="col-md-12 ftco-animate">
                    <div class="cart-list">
                        <table class="table">
                            <thead class="thead-primary">
                            <tr class="text-center">
                                <th>Title</th>
                                <th>Link</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <?php
                            if($pts)
                            {
                            foreach($pts as $pt)
                            {
                            ?>
                            <tbody>
                            <tr class="text-center">
                                <td>
                                    <h3><a href="view.php?id=<?php echo  $pt['id']; ?>"><?php echo  $pt['name'];?></a></h3>
                                </td>
<td>
    <h3><?php echo  $pt['link'];?></h3>

</td>
                                <td><a href="<?=VIEW;?>popular_tag/edit.php?id=<?php echo  $pt['id']; ?>">Edit</a>|
                                    <a href="<?=VIEW;?>popular_tag/delete.php?id=<?php echo  $pt['id']; ?>">Delete</a></td>
                            </tr><!-- END TR-->

                            <?php
                            }}
                            else
                            {
                                ?>
                                <tr><td>There is no more data<a href="add.php"> click here</a> for add data</td></tr>
                            <?php }?>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </main>

<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>