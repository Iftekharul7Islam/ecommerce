<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');
//selection query
$query = "SELECT * FROM testimonials";
$sth = $conn->prepare($query);
$sth->execute();

/* Fetch all of the remaining rows in the result set */
$testimonials = $sth->fetchAll(PDO::FETCH_ASSOC);
//print_r($testimonials);
?>
<?php
ob_start();
?>



        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
                <h1>Testimonial</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <button type="button" class="btn btn-sm btn-outline-secondary">
                        <span data-feather="calendar"></span>
                        <a href="add.php" style="color: black">Add New</a>
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 ftco-animate">
                    <div class="cart-list">
                        <table class="table">
                            <thead class="thead-primary">
                            <tr class="text-center">
                                <th>&nbsp;</th>
                                <th>Picture</th>
                                <th>Body</th>
                                <th>Name</th>
                                <th>Designation</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if($testimonials){
                            foreach($testimonials as $testimonial)
                            {
                            ?>
                            <tr class="text-center">
                                <td class="testimonial-sl"><a href="#"><span class="ion-ios-close"></span></a></td>

                                <td class="image-test">
                                    <div class="img"><img src="<?php echo $testimonial['picture']; ?>"width="140px" height="120px">
                                    </div>

                                <td class="testimonial-body">
                                    <h3><?php echo $testimonial['body']; ?></h3>
                                </td>
                                <td class="testimonial-name">
                                    <h3><a href="view.php?id=<?php echo $testimonial['id'];?>"><?php echo $testimonial['name']; ?></a></h3>

                                </td>

                                <td class="designation"><?php echo $testimonial['designation']; ?></td>
                                <td><a href="<?=VIEW;?>testimonials/edit.php?id=<?php echo $testimonial['id']?>">Edit </a>
                                    | <a href="<?=VIEW;?>testimonials/delete.php?id=<?php echo $testimonial['id']?>">Delete</a></td>
                            </tr>
                            <?php } } else{
                                ?>
                                <tr class="text-center">
                                    <td colspan="6">there is no testimonial</td></tr>
                              <?php
                            }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>