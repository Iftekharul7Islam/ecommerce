<?php
//collect the data
$picture=$_POST['picture'];
$body=$_POST['body'];
$name=$_POST['name'];
$designation=$_POST['designation'];

//.....connect to database

include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//prepare the insert query
$query = "INSERT INTO `testimonials` 
( `picture`, 
`body`, `name`, `designation`,
 `is_active`, `is_draft`, `soft_delete`, `created_at`,
  `modified_at`) VALUES ( :picture, :body, :name, :designation, NULL,
   NULL, NULL, NULL, NULL);";

$sth = $conn->prepare($query);
$sth->bindParam(':picture',$picture);
$sth->bindParam(':body',$body);
$sth->bindParam(':name',$name);
$sth->bindParam(':designation',$designation);
$result=$sth->execute();
//print_r($result);
$sth=null;

//redirect to index page
header("location:index.php");