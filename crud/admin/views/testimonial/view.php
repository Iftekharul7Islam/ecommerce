<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//selection query
$query = "SELECT * FROM testimonials WHERE id=".$_GET['id'];
$sth = $conn->prepare($query);
$sth->execute();

/* Fetch all of the remaining rows in the result set */
$testimonial= $sth->fetch(PDO::FETCH_ASSOC);
//print_r($testimonial);
?>
<?php
ob_start();
?>


        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1> Testimonial</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <button type="button" class="btn btn-sm btn-outline-secondary">
                        <span data-feather="calendar"></span>
                        <a href="add.php" style="color: black">Add New</a>
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 ftco-animate">


<section >
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm col-md-6 col-lg-3 ftco-animate">
                <div class="product">
                    <a href="#" class="img-test"><img class="img-fluid" src="<?php echo $testimonial['picture']; ?>" alt="Colorlib Template">
                    </a>
                    <div class="text py-3 px-3">
                        <h3><a href="#"><?php echo $testimonial['name'] ?></a></h3>
                        <div class="d-flex">
                            <div class="designation">
                                <p class="desig"><?php echo $testimonial['designation']; ?></p>
                            </div>
                        </div>
                        <hr>
                        <!--<p class="bottom-area d-flex">
                            <a href="#" class="add-to-cart"><span>Add to cart <i class="ion-ios-add ml-1"></i></span></a>
                            <a href="#" class="ml-auto"><span><i class="ion-ios-heart-empty"></i></span></a>
                        </p>-->
                    </div>
                </div>
            </div>
       <!--     <div class="col-sm col-md-6 col-lg-3 ftco-animate">
                <div class="product">
                    <a href="#" class="img-prod"><img class="img-fluid" src="../../lib/img/images.jpg" alt="Colorlib Template">
                        <span class="status">New Arrival</span>
                    </a>
                    <div class="text py-3 px-3">
                        <h3><a href="#">Young Woman Wearing Dress</a></h3>
                        <div class="d-flex">
                            <div class="pricing">
                                <p class="price"><span>$120.00</span></p>
                            </div>
                            <div class="rating">
                                <p class="text-right">
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                </p>
                            </div>
                        </div>
                        <hr>
                        <p class="bottom-area d-flex">
                            <a href="#" class="add-to-cart"><span>Add to cart <i class="ion-ios-add ml-1"></i></span></a>
                            <a href="#" class="ml-auto"><span><i class="ion-ios-heart-empty"></i></span></a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm col-md-6 col-lg-3 ftco-animate">
                <div class="product">
                    <a href="#" class="img-prod"><img class="img-fluid" src="../../lib/img/images.jpg" alt="Colorlib Template"></a>
                    <div class="text py-3 px-3">
                        <h3><a href="#">Young Woman Wearing Dress</a></h3>
                        <div class="d-flex">
                            <div class="pricing">
                                <p class="price"><span>$120.00</span></p>
                            </div>
                            <div class="rating">
                                <p class="text-right">
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                </p>
                            </div>
                        </div>
                        <hr>
                        <p class="bottom-area d-flex">
                            <a href="#" class="add-to-cart"><span>Add to cart <i class="ion-ios-add ml-1"></i></span></a>
                            <a href="#" class="ml-auto"><span><i class="ion-ios-heart-empty"></i></span></a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm col-md-6 col-lg-3 ftco-animate">
                <div class="product">
                    <a href="#" class="img-prod"><img class="img-fluid" src="../../lib/img/images.jpg" alt="Colorlib Template"></a>
                    <div class="text py-3 px-3">
                        <h3><a href="#">Young Woman Wearing Dress</a></h3>
                        <div class="d-flex">
                            <div class="pricing">
                                <p class="price"><span>$120.00</span></p>
                            </div>
                            <div class="rating">
                                <p class="text-right">
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                </p>
                            </div>
                        </div>
                        <hr>
                        <p class="bottom-area d-flex">
                            <a href="#" class="add-to-cart"><span>Add to cart <i class="ion-ios-add ml-1"></i></span></a>
                            <a href="#" class="ml-auto"><span><i class="ion-ios-heart-empty"></i></span></a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm col-md-6 col-lg-3 ftco-animate">
                <div class="product">
                    <a href="#" class="img-prod"><img class="img-fluid" src="../../lib/img/images.jpg" alt="Colorlib Template"></a>
                    <div class="text py-3 px-3">
                        <h3><a href="#">Young Woman Wearing Dress</a></h3>
                        <div class="d-flex">
                            <div class="pricing">
                                <p class="price"><span>$120.00</span></p>
                            </div>
                            <div class="rating">
                                <p class="text-right">
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                </p>
                            </div>
                        </div>
                        <hr>
                        <p class="bottom-area d-flex">
                            <a href="#" class="add-to-cart"><span>Add to cart <i class="ion-ios-add ml-1"></i></span></a>
                            <a href="#" class="ml-auto"><span><i class="ion-ios-heart-empty"></i></span></a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm col-md-6 col-lg-3 ftco-animate">
                <div class="product">
                    <a href="#" class="img-prod"><img class="img-fluid" src="../../lib/img/images.jpg" alt="Colorlib Template"></a>
                    <div class="text py-3 px-3">
                        <h3><a href="#">Young Woman Wearing Dress</a></h3>
                        <div class="d-flex">
                            <div class="pricing">
                                <p class="price"><span>$120.00</span></p>
                            </div>
                            <div class="rating">
                                <p class="text-right">
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                </p>
                            </div>
                        </div>
                        <hr>
                        <p class="bottom-area d-flex">
                            <a href="#" class="add-to-cart"><span>Add to cart <i class="ion-ios-add ml-1"></i></span></a>
                            <a href="#" class="ml-auto"><span><i class="ion-ios-heart-empty"></i></span></a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm col-md-6 col-lg-3 ftco-animate">
                <div class="product">
                    <a href="#" class="img-prod"><img class="img-fluid" src="../../lib/img/images.jpg" alt="Colorlib Template"></a>
                    <div class="text py-3 px-3">
                        <h3><a href="#">Young Woman Wearing Dress</a></h3>
                        <div class="d-flex">
                            <div class="pricing">
                                <p class="price"><span>$120.00</span></p>
                            </div>
                            <div class="rating">
                                <p class="text-right">
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                </p>
                            </div>
                        </div>
                        <hr>
                        <p class="bottom-area d-flex">
                            <a href="#" class="add-to-cart"><span>Add to cart <i class="ion-ios-add ml-1"></i></span></a>
                            <a href="#" class="ml-auto"><span><i class="ion-ios-heart-empty"></i></span></a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm col-md-6 col-lg-3 ftco-animate">
                <div class="product">
                    <a href="#" class="img-prod"><img class="img-fluid" src="../../lib/img/images.jpg" alt="Colorlib Template">
                        <span class="status">Best Sellers</span>
                    </a>
                    <div class="text py-3 px-3">
                        <h3><a href="#">Young Woman Wearing Dress</a></h3>
                        <div class="d-flex">
                            <div class="pricing">
                                <p class="price"><span>$120.00</span></p>
                            </div>
                            <div class="rating">
                                <p class="text-right">
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                    <span class="ion-ios-star-outline"></span>
                                </p>
                            </div>
                        </div>
                        <hr>
                        <p class="bottom-area d-flex">
                            <a href="#" class="add-to-cart"><span>Add to cart <i class="ion-ios-add ml-1"></i></span></a>
                            <a href="#" class="ml-auto"><span><i class="ion-ios-heart-empty"></i></span></a>
                        </p>
                    </div>
                </div>
            </div>-->
        </div>
    </div>
</section>
                </main>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>
