<?php

include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//selection query
$query = "SELECT * FROM testimonials WHERE id=".$_GET['id'];
$sth = $conn->prepare($query);
$sth->execute();

/* Fetch all of the remaining rows in the result set */
$testimonial= $sth->fetch(PDO::FETCH_ASSOC);
//print_r($testimonial);
?>
<?php
ob_start();
?>


        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">



            <form id="testimonial-form" method="post" action="update.php" role="form">

                <div class="messages"></div>
                <h1>Edit</h1>
                <div class="controls">
                    <div class="row">
                        <div class="col-lg-12">
                        <div class="form-group">
                      <!--      <label for="id">ID</label>-->
                            <input id="id"  value="<?php echo $testimonial['id'] ?>" type="hidden" name="id" class="form-control">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="picture">Picture</label>
                                <input id="picture"  value="<?php echo $testimonial['picture'] ?>" type="text" name="picture" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="body">Body</label>
                                <input id="body"  value="<?php echo $testimonial['body'] ?>" type="text" name="body" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input id="name"  value="<?php echo $testimonial['name'] ?>" type="text" name="name" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="designation">designation</label>
                                <input id="designation"  value="<?php echo $testimonial['designation'] ?>" type="text" name="designation" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                       <!-- <div class="col-lg-6">
                            <div class="form-group">
                                <label for="test_is_active ">test_is_active </label>
                                <input id="test_is_active "  value="" type="text" name="test_is_active " class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="test_is_draft ">test_is_draft </label>
                                <input id="test_is_draft "  value="" type="text" name="test_is_draft " class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="test_soft_delete ">test_soft_delete </label>
                                <input id="test_soft_delete "  value="" type="text" name="test_soft_delete " class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="test_created_at ">test_created_at </label>
                                <input id="test_created_at "  value="" type="text" name="test_created_at " class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="test_modified_at">test_modified_at</label>
                                <input id="test_modified_at"  value="" type="text" name="test_modified_at" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>-->
                    </div>

                    <button type="submit" class="btn btn-success">Send & Save Testimonial</button>
                    <!--<input type="submit" class="btn btn-success btn-send" value="Send & Save message">-->


                </div>

            </form>
        </main>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>
