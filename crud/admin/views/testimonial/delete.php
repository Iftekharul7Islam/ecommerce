<?php
//collect the data
$id=$_GET['id'];



//connecting to database
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//prepare the insert query
$query = "DELETE FROM `testimonials` WHERE `testimonials`.`id` = :id;";

$sth = $conn->prepare($query);
$sth->bindParam(':id',$id);

$result=$sth->execute();
//print_r($result);
$sth=null;

//redirect to index page
header("location:index.php");