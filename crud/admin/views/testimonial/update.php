<?php
//collect the data
$id=$_POST['id'];
$picture=$_POST['picture'];
$body=$_POST['body'];
$name=$_POST['name'];
$designation=$_POST['designation'];

include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');
//prepare the insert query
$query = "UPDATE `testimonials` SET
 `picture` = :picture, `body` = :body, `name` = :name, `designation` = :designation
 WHERE `testimonials`.`id` = :id;";

$sth = $conn->prepare($query);
$sth->bindParam(':id',$id);
$sth->bindParam(':picture',$picture);
$sth->bindParam(':body',$body);
$sth->bindParam(':name',$name);
$sth->bindParam(':designation',$designation);
$result=$sth->execute();
//print_r($result);
$sth=null;

//redirect to index page
header("location:index.php");