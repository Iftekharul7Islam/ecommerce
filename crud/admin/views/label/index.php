<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//selection query
$query = "SELECT * FROM labels";
$sth = $conn->prepare($query);
$sth->execute();

$labels = $sth->fetchAll(PDO::FETCH_ASSOC);

?>

<?php
ob_start();
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-12 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Label</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <button type="button" class="btn btn-sm btn-outline-secondary">
                <span data-feather="calendar"></span>
                <a href="<?=VIEW;?>label/create.php" style="color: black">Add New</a>
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ftco-animate">
            <div class="cart-list">
                <table class="table">
                    <thead class="thead-primary">
                    <tr class="text-center">
                        <th>&nbsp;</th>
                        <th>Label Picture</th>
                        <th>Label Title</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($labels){
                        foreach ($labels as $label){
                            ?>
                            <tr class="text-center">
                                <td class="brand-sl"><a href="#"><span class="ion-ios-close"></span></a></td>

                                <td class="image-prod"><div class="img">
                                        <img src="<?php echo $label['picture'];?>" width="140px" height="120px"></div>
                                </td>

                                <td class="product-name">
                                    <h3><a href="show.php?id=<?php echo $label['id'];?>"><?php echo $label['title'];?></a></h3>
                                </td>
                                <td><a href="edit.php?id=<?php echo $label['id'];?>">Edit</a> |
                                    <a href="delete.php?id=<?php echo $label['id'];?>">Delete</a></td>
                            </tr>
                        <?php } }else{?>
                        <tr class="text-center">
                            <td colspan="6">there is no label available <a href="create.php">click here</a> to add a label</td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>