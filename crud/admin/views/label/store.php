<?php
//collect the data
$title = $_POST['title'];
$picture = $_POST['picture'];

include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//prepare the insert query
$query = "INSERT INTO `labels` (
`title`, 
`picture`) 
VALUES ( :title, :picture)";

$sth = $conn->prepare($query);

$sth->bindParam(':title', $title);
$sth->bindParam(':picture', $picture);

$sth->execute();

//redirect to index page
header('location:index.php');