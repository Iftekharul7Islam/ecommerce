<?php
//collect the id
$id = $_GET['id'];

include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//selection query
$query = "SELECT * FROM labels WHERE id = :id";

$sth = $conn->prepare($query);
$sth->bindParam(':id', $id);
$sth->execute();

$label = $sth->fetch(PDO::FETCH_ASSOC);

?>

<?php
ob_start();
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-12 px-4">

    <form id="labels-entry-form" method="post" action="update.php" role="form">

        <div class="messages"></div>
        <h1>Edit Label</h1>
        <div class="controls">
            <div class="row">

                <input id="id"
                       value="<?php echo $label['id'];?>"
                       type="hidden"
                       name="id"
                       class="form-control">

                <div class="col-lg-8">
                    <div class="form-group">
                        <label for="title">Label Name</label>
                        <input id="title"
                               value="<?php echo $label['title'];?>"
                               type="text"
                               name="title"
                               autofocus="autofocus"
                               placeholder="e.g. Pahela Baishakh"
                               class="form-control">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="col-lg-8">
                    <div class="form-group">
                        <label for="picture">Label Picture</label>
                        <input id="picture"
                               value="<?php echo $label['picture'];?>"
                               type="text"
                               name="picture"
                               placeholder="e.g. link of a picture"
                               class="form-control">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

            </div>

            <button type="submit" class="btn btn-success">
                Send & Save Brand
            </button>

        </div>

    </form>
</main>

<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>
