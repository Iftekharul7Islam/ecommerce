<?php
//collect the data
$id = $_POST['id'];
$title = $_POST['title'];
$picture = $_POST['picture'];

include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//prepare the update query
$query = "UPDATE `labels` SET 
`title` = :title, 
`picture` = :picture
WHERE `labels`.`id` = :id";

$sth = $conn->prepare($query);

$sth->bindParam(':id', $id);
$sth->bindParam(':title', $title);
$sth->bindParam(':picture', $picture);

$sth->execute();

//redirect to index page
header('location:index.php');