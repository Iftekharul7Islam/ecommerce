<?php
//connect to database
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

$query="select * from sponsers where id=".$_GET['id'];
$sth = $conn->prepare($query);
$sth->execute();
$sponser=$sth->fetch(PDO::FETCH_ASSOC);
//print_r($sponser);
?>
<?php
ob_start();
?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">



            <form id="contact-form" method="post" action="update.php" role="form">
                <input id="id"  value="<?php echo $sponser['id']?>"
                       type="hidden" name="id" class="form-control">
                <div class="messages"></div>
                <h1>Edit</h1>
                <div class="controls">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">


                                &nbsp;
                            </div>


                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input id="title"
                                           value="<?php echo $sponser['title'];?>"
                                           type="text"
                                           name="title"
                                           placeholder="e.g.samsung" class="form-control"
                                           autofocus="autofocus";>

                                    <div class="help-block text-muted">Enter Sponser Title</div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="picture">picture</label>
                                        <input id="picture"
                                               value=""
                                               type="file"
                                               name="picture"
                                               placeholder="" class="form-control"
                                               autofocus="autofocus";>


                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="pm">promotional_message</label>
                                        <input id="pm"
                                               value="<?php echo $sponser['promotional_message'];?>"
                                               type="text"
                                               name="pm"
                                               placeholder="" class="form-control"
                                               autofocus="autofocus";>


                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="link">link</label>
                                        <input id="link"
                                               value="<?php echo $sponser['link'];?>"
                                               type="text"
                                               name="link"
                                               placeholder="" class="form-control"
                                               autofocus="autofocus";>


                                    </div>
                                </div>

                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="hb">html_banner:</label>
                                    <input id="hb"
                                           value="<?php echo $sponser['html_banner'];?>"
                                           type="text"
                                           name="hb"
                                           placeholder="" class="form-control"
                                           autofocus="autofocus";>


                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="ia">Is_active</label>
                                    <input id="ia"
                                           value="<?php echo $sponser['is_active'];?>"
                                           type="text"
                                           name="ia"
                                           placeholder="" class="form-control"
                                           autofocus="autofocus";>


                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="id">Is_draft</label>
                                    <input id="id"
                                           value="<?php echo $sponser['is_draft'];?>"
                                           type="text"
                                           name="ia"
                                           placeholder="" class="form-control"
                                           autofocus="autofocus";>


                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="sd">soft_delete:</label>
                                    <input id="id"
                                           value="<?php echo $sponser['soft_delete'];?>"
                                           type="text"
                                           name="sd"
                                           placeholder="" class="form-control"
                                           autofocus="autofocus";>


                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="ca">created_at:</label>
                                    <input id="ca"
                                           value="<?php echo $sponser['created_at'];?>"
                                           type="datetime"
                                           name="ca"
                                           placeholder="" class="form-control"
                                           autofocus="autofocus";>


                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="md">modified_at:</label>
                                    <input id="md"
                                           value="<?php echo $sponser['modified_at'];?>"
                                           type="datetime"
                                           name="ca"
                                           placeholder="" class="form-control"
                                           autofocus="autofocus";>


                                </div>
                            </div>

                    </div>
                </div>

                <button type="submit" class="btn btn-success">Send & Save</a></button>

    </div>

    </form>
    </main>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>
