<?php
//connect to database
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');
$query="select * from sponsers";
$sth = $conn->prepare($query);
$sth->execute();
$sponsers=$sth->fetchAll(PDO::FETCH_ASSOC);

?>
<?php
ob_start();
?>



        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Sponser</h1>
                <button type="button" class="btn btn-sm btn-outline-secondary">
                        <span data-feather="calendar"></span>
                        <a href="add.php" style="color: black">Add New</a>
                    </button>

            </div>
            <div class="row">
                <div class="col-md-12 ftco-animate">
                    <div class="cart-list">
                        <table class="table">
                            <thead class="thead-primary">
                            <tr class="text-center">
                                <th>Picture</th>
                                <th>Title</th>
                                <th>Promotional_messages</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <?php
                            if($sponsers)
                            {
                            foreach($sponsers as $sponser)
                            {
                            ?>
                            <tbody>
                            <tr class="text-center">

                                <td class="image-prod"><div class="img"><img src="<?php echo $sponser['picture'];?>" width="140px" height="120px"></div></td>


                                <td>
                                    <h3><a href="view.php?id=<?php echo  $sponser['id']; ?>"><?php echo  $sponser['title'];?></a></h3>
                                </td>
                           <td class="text-center">
                               <h3><?php echo  $sponser['promotional_message'];?></h3>

                                </td>

                                <td><a href="<?=VIEW;?>sponsers/edit.php?id=<?php echo  $sponser['id']; ?>">Edit</a>|
                                    <a href="<?=VIEW;?>sponsers/delete.php?id=<?php echo  $sponser['id']; ?>">Delete</a></td>
                            </tr><!-- END TR-->

                            <?php
                            }}
                            else
                            {
                                ?>
                            <tr><td>There is no more data<a href="add.php"> click here</a> for add data</td></tr>
                            <?php }?>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </main>



<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>
