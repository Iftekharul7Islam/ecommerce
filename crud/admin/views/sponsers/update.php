<?php
$id=$_POST['id'];
$title=$_POST['title'];
$picture=$_POST['picture'];
$link=$_POST['link'];
$promotional_message=$_POST['promotional_message'];
$html_banner=$_POST['html_banner'];
$is_active=$_POST['is_active'];
$is_draft=$_POST['is_draft'];
$soft_delete=$_POST['soft_delete'];
$created_at=$_POST['created_at'];
$modified_at=$_POST['modified_at'];


include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

$query="UPDATE `sponsers`
 SET `title` = :title, 
 `link` = :link, 
 `promotional_message` = :promotional_message, 
 `html_banner` = :html_banner,
  `is_active` = :is_active, 
  `is_draft` = :is_draft, 
  `soft_delete` = :soft_delete, 
  `created_at` = :created_at, 
  `modified_at` = :modified_at WHERE `sponsers`.`id` = :id;";
$sth = $conn->prepare($query);
$sth->bindparam(':id',$id);
$sth->bindparam(':title',$title);
$sth->bindparam(':picture',$picture);
$sth->bindparam(':link',$link);
$sth->bindparam(':promotional_message',$promotional_message);
$sth->bindparam(':html_banner',$html_banner);
$sth->bindparam(':is_active',$is_active);
$sth->bindparam(':is_draft',$is_draft);
$sth->bindparam(':soft_delete',$soft_delete);
$sth->bindparam(':created_at',$created_at);
$sth->bindparam(':modified_at',$modified_at);

$result=$sth->execute();
header("location:index.php");