<?php
//collect the id
$id = $_GET['id'];

include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//selection query
$query = "SELECT * FROM products WHERE id = :id";

$sth = $conn->prepare($query);
$sth->bindParam(':id', $id);
$sth->execute();

$product = $sth->fetch(PDO::FETCH_ASSOC);

?>


<?php
ob_start();
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-12 px-4">

            <form id="products-entry-form" method="post" action="update.php" role="form">

                <div class="messages"></div>
                <h1>Edit Products</h1>
                <div class="controls">
                    <div class="row">
                        
                        <input id="id"
                               value="<?php echo $product['id'];?>"
                               type="hidden"
                               name="id"
                               class="form-control">

                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input id="title"
                                       value="<?php echo $product['title'];?>"
                                       type="text"
                                       name="title"
                                       autofocus="autofocus"
                                       placeholder="e.g. Bashundhara Tissue"
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="brand">Brand Name</label>
                                <input id="brand"
                                       value="<?php echo $product['brand'];?>"
                                       type="text"
                                       name="brand"
                                       placeholder="e.g. Bashundhara"
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="category">Category</label>
                                <input id="category"
                                       value="<?php echo $product['category'];?>"
                                       type="text"
                                       name="category"
                                       placeholder="e.g. Pantry & Cleaning"
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="picture">Picture</label>
                                <input id="picture"
                                       value="<?php echo $product['picture'];?>"
                                       type="text"
                                       name="picture"
                                       placeholder="e.g. link of a picture"
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="short_description">Short Description</label>
                                <input id="short_description"
                                       value="<?php echo $product['short_description'];?>"
                                       type="text"
                                       name="short_description"
                                       placeholder="e.g. Lorem Ipsum is simply dummy text"
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <input id="description"
                                       value="<?php echo $product['description'];?>"
                                       type="text"
                                       name="description"
                                       placeholder="e.g. Lorem Ipsum is simply dummy text of the printing and typesetting industry."
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="cost">Cost</label>
                                <input id="cost"
                                       value="<?php echo $product['cost'];?>"
                                       type="text"
                                       name="cost"
                                       placeholder="e.g. 85.36BDT"
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="mrp">mrp</label>
                                <input id="mrp"
                                       value="<?php echo $product['mrp'];?>"
                                       type="text"
                                       name="mrp"
                                       placeholder="e.g. 84.36BDT"
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="special_price">Special Price</label>
                                <input id="special_price"
                                       value="<?php echo $product['special_price'];?>"
                                       type="text"
                                       name="special_price"
                                       placeholder="e.g. 80.00BDT"
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        
                    </div>

                    <button type="submit" class="btn btn-success">
                        Send & Save Product
                    </button>

                </div>

            </form>
        </main>

<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>
