<?php
$title = $_POST['title'];
$brand = $_POST['brand'];
$category = $_POST['category'];
$category = $_POST['category'];
$picture = $_POST['picture'];
$short_description = $_POST['short_description'];
$description = $_POST['description'];
$cost = $_POST['cost'];
$mrp = $_POST['mrp'];
$special_price = $_POST['special_price'];

include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//prepare the insert query
$query = "INSERT INTO `products`
(
`brand`,
`category`,
`title`,
`picture`,
`short_description`,
`description`,
`cost`,
`mrp`,
`special_price`)
VALUES
(
    :brand,
    :category,
    :title,
    :picture,
    :short_description,
    :description, 
    :cost, 
    :mrp, 
    :special_price)";

$sth = $conn->prepare($query);

$sth->bindParam(':title', $title);
$sth->bindParam(':brand', $brand);
$sth->bindParam(':category', $category);
$sth->bindParam(':picture', $picture);
$sth->bindParam(':short_description', $short_description);
$sth->bindParam(':description', $description);
$sth->bindParam(':cost', $cost);
$sth->bindParam(':mrp', $mrp);
$sth->bindParam(':special_price', $special_price);

$result = $sth->execute();


//redirect to index page
header('location:index.php');
