<?php
//collect the data
$id = $_POST['id'];
$title = $_POST['title'];
$brand = $_POST['brand'];
$category = $_POST['category'];
$picture = $_POST['picture'];
$short_description = $_POST['short_description'];
$description = $_POST['description'];
$cost = $_POST['cost'];
$mrp = $_POST['mrp'];
$special_price = $_POST['special_price'];

include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//prepare the insert query
$query = "UPDATE `products` SET 
`brand` = :brand, 
`category` = :category, 
`title` = :title, 
`picture` = :picture, 
`short_description` = :short_description, 
`description` = :description, 
`cost` = :cost, 
`mrp` = :mrp, 
`special_price` = :special_price
WHERE 
`products`.`id` = :id";

$sth = $conn->prepare($query);

$sth->bindParam(':id', $id);
$sth->bindParam(':title', $title);
$sth->bindParam(':brand', $brand);
$sth->bindParam(':category', $category);
$sth->bindParam(':picture', $picture);
$sth->bindParam(':short_description', $short_description);
$sth->bindParam(':description', $description);
$sth->bindParam(':cost', $cost);
$sth->bindParam(':mrp', $mrp);
$sth->bindParam(':special_price', $special_price);

$sth->execute();

//redirect to index page
header('location:index.php');
