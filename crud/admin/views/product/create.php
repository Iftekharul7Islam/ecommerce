<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');
?>

<?php
ob_start();
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-12 px-4">
    
            <form id="product-entry-form" method="post" action="store.php" role="form">

                <div class="messages"></div>
                <h1>ADD NEW PRODUCT</h1>
                <div class="controls">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input id="title"
                                       value=""
                                       type="text"
                                       name="title"
                                       autofocus="autofocus"
                                       placeholder="e.g. Bashundhara Tissue"
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="brand">Brand Name</label>
                                <input id="brand"
                                       value=""
                                       type="text"
                                       name="brand"
                                       placeholder="e.g. Bashundhara"
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="category">Category</label>
                                <input id="category"
                                       value=""
                                       type="text"
                                       name="category"
                                       placeholder="e.g. Pantry & Cleaning"
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="picture">Picture</label>
                                <input id="picture"
                                       value=""
                                       type="text"
                                       name="picture"
                                       placeholder="e.g. link of a picture"
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="short_description">Short Description</label>
                                <input id="short_description"
                                       value=""
                                       type="text"
                                       name="short_description"
                                       placeholder="e.g. Lorem Ipsum is simply dummy text"
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <input id="description"
                                       value=""
                                       type="text"
                                       name="description"
                                       placeholder="e.g. Lorem Ipsum is simply dummy text of the printing and typesetting industry."
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="cost">Cost</label>
                                <input id="cost"
                                       value=""
                                       type="text"
                                       name="cost"
                                       placeholder="e.g. 85.36BDT"
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="mrp">mrp</label>
                                <input id="mrp"
                                       value=""
                                       type="text"
                                       name="mrp"
                                       placeholder="e.g. 84.36BDT"
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="special_price">Special Price</label>
                                <input id="special_price"
                                       value=""
                                       type="text"
                                       name="special_price"
                                       placeholder="e.g. 80.00BDT"
                                       class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>

<button type="submit" class="btn btn-success">
    Send & Save Product</a>
</button>


                </div>

            </form>
        </main>

<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>
