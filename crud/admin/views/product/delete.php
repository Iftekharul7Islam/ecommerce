<?php
//collect the id
$id = $_GET['id'];

include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//prepare the delete query
$query = "DELETE FROM `products` WHERE `products`.`id` = :id";

$sth = $conn->prepare($query);
$sth->bindParam(':id', $id);
$result = $sth->execute();

//redirect to index page
header('location:index.php');