<?php
//collect the data
$id = $_POST['id'];
$name = $_POST['admin_name'];
$email = $_POST['admin_email'];
$password = $_POST['admin_password'];
$phone = $_POST['admin_phone'];

include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//prepare the update query
$query = "UPDATE `admins` SET 
`name` = :name, 
`email` = :email, 
`password` = :password,
`phone` = :phone
WHERE `admins`.`id` = :id";

/*print_r($query);
echo '<hr/>';*/
$sth = $conn->prepare($query);

$sth->bindParam(':id', $id);
/*print_r($sth);
echo '<hr/>';*/
$sth->bindParam(':name', $name);
/*print_r($sth);
echo '<hr/>';*/
$sth->bindParam(':email', $email);
/*print_r($sth);
echo '<hr/>';*/
$sth->bindParam(':password', $password);
/*print_r($sth);
echo '<hr/>';*/
$sth->bindParam(':phone', $phone);
/*print_r($sth);
echo '<hr/>';*/

$sth->execute();

//redirect to index page
header('location:index.php');