<?php

/*print_r($_POST);
echo '<hr/>';
var_dump($_POST);
die();
*/

//collect the data
$name = $_POST['admin_name'];
/*print_r($name);
echo '<hr/>';*/
$email = $_POST['admin_email'];
/*print_r($email);
echo '<hr/>';*/
$password = $_POST['admin_password'];
/*print_r($password);
echo '<hr/>';*/
$phone = $_POST['admin_phone'];
/*print_r($phone);
echo '<hr/>';
echo '<hr/>';*/

include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//prepare the insert query
$query = "INSERT INTO `admins` 
(
`name`, 
`email`, 
`password`, 
`phone`) 
VALUES 
(
:name, 
:email, 
:password, 
:phone)";

/*print_r($query);
echo '<hr/>';*/
$sth = $conn->prepare($query);
/*print_r($sth);
echo '<hr/>';*/
$sth->bindParam(':name', $name);
/*print_r($sth);
echo '<hr/>';*/
$sth->bindParam(':email', $email);
/*print_r($sth);
echo '<hr/>';*/
$sth->bindParam(':password', $password);
/*print_r($sth);
echo '<hr/>';*/
$sth->bindParam(':phone', $phone);
/*print_r($sth);
echo '<hr/>';*/

$sth->execute();

//redirect to index page
header('location:index.php');