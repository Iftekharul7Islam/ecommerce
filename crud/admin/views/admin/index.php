<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

//selection query
$query = "SELECT * FROM admins";
$sth = $conn->prepare($query);
$sth->execute();

$admins = $sth->fetchAll(PDO::FETCH_ASSOC);

?>

<?php
ob_start();
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-12 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Admin Panel</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <button type="button" class="btn btn-sm btn-outline-secondary">
                <span data-feather="calendar"></span>
                <a href="<?=VIEW;?>admin/create.php" style="color: black">Add New</a>
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ftco-animate">
            <div class="cart-list">
                <table class="table">
                    <thead class="thead-primary">
                    <tr class="text-center">
                        <th>&nbsp;</th>
                        <th>Name</th>
                        <th>Email Address</th>
                        <th>Phone Number</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($admins){
                        foreach ($admins as $admin){
                            ?>
                            <tr class="text-center">
                                <td class="admin-sl"><a href="#"><span class="ion-ios-close"></span></a></td>

                               <!-- <td class="image-prod">
                                    <div class="img"><img src="<?php /*echo $admin['picture'];*/?>" width="140px" height="120px">
                                    </div>
                                </td>-->

                                <td class="admin-name">
                                    <h3><a href="show.php?id=<?php echo $admin['id'];?>"><?php echo $admin['name'];?></a></h3>
                                </td>
                                <td class="admin-email">
                                    <p><?php echo $admin['email'];?></p>
                                </td>
                                <td class="admin-phone">
                                    <p><?php echo $admin['phone'];?></p>
                                </td>

                                <td><a href="edit.php?id=<?php echo $admin['id'];?>">Edit</a> |
                                    <a href="delete.php?id=<?php echo $admin['id'];?>">Delete</a>
                                </td>
                            </tr>
                        <?php } }else{

                        ?>
                        <tr class="text-center">
                            <td colspan="6">there is no admin available <a href="create.php">click here</a> to add an admin</td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>


<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>