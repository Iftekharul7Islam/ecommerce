<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');
?>

<?php
ob_start();
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-12 px-4">

    <form id="admins-entry-form" method="post" action="store.php" role="form">

        <div class="messages"></div>
        <h1>ADD NEW ADMIN</h1>
        <div class="controls">
            <div class="row">

                <div class="col-lg-8">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input id="name"
                               value=""
                               type="text"
                               name="admin_name"
                               autofocus="autofocus"
                               placeholder="e.g. John Doe"
                               class="form-control">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input id="email"
                               value=""
                               type="email"
                               name="admin_email"
                               placeholder="e.g. john.doe@gmail.com"
                               class="form-control">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="form-group">
                        <label for="password ">Password</label>
                        <input id="password"
                               value=""
                               type="password"
                               name="admin_password"
                               placeholder="e.g. alpha numeric value and more than 6 digits"
                               class="form-control">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input id="phone "
                               value=""
                               type="text"
                               name="admin_phone"
                               placeholder="e.g. 018XXXXXXXX"
                               class="form-control">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

            </div>

            <button type="submit" class="btn btn-success">
                Send & Save Admin
            </button>

        </div>

    </form>
</main>

<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>