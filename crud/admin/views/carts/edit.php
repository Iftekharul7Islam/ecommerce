<?php
//connect to database
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

$query="select * from carts where id=".$_GET['id'];
$sth = $conn->prepare($query);
$sth->execute();
$carts=$sth->fetch(PDO::FETCH_ASSOC);
//print_r($sponser);
?>
<?php
ob_start();
?>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">



    <form id="contact-form" method="post" action="update.php" role="form">
        <input id="id"  value="<?php echo $carts['id']?>"
               type="hidden" name="id" class="form-control">
        <div class="messages"></div>
        <h1>Edit</h1>
        <div class="controls">
            <div class="row">

                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="sid">Sid</label>
                        <input id="sid"
                               value="<?php echo $carts['sid']?>"
                               type="text"
                               name="sid"
                               placeholder="" class="form-control"
                               autofocus="autofocus";>

                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="product_id">product_id</label>
                        <input id="product_id"
                               value="<?php echo $carts['product_id']?>"
                               type="text"
                               name="product_id
                               placeholder="" class="form-control"
                        autofocus="autofocus";>

                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="picture">picture</label>
                        <input id="picture"
                               value="<?php echo $carts['picture']?>"
                               type="file"
                               name="picture"
                               placeholder="" class="form-control"
                               autofocus="autofocus";>


                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="product_title">Title</label>
                        <input id="product_title"
                               value="<?php echo $carts['product_title']?>"
                               type="text"
                               name="product_title"
                               placeholder="e.g.samsung" class="form-control"
                               autofocus="autofocus";>

                        <div class="help-block text-muted">Enter carts Title</div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="qty">quantity</label>
                        <input id="qty"
                               value="<?php echo $carts['qty']?>"
                               type="text"
                               name="qty"
                               placeholder="" class="form-control"
                               autofocus="autofocus";>


                    </div>
                </div>


                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="unit_price">Unit_pice:</label>
                        <input id="unit_price"
                               value="<?php echo $carts['unit_price']?>"
                               type="text"
                               name="unit_price"
                               placeholder="" class="form-control"
                               autofocus="autofocus";>


                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="total_price">total_pice:</label>
                        <input id="total_price"
                               value="<?php echo $carts['total_price']?>"
                               type="text"
                               name="total_price"
                               placeholder="" class="form-control"
                               autofocus="autofocus";>


                    </div>
                </div>

            </div>
        </div>
                <button type="submit" class="btn btn-success">send & Save</a></button>


    </form>
</main>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>
