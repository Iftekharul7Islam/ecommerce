
<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');
?>

<?php
ob_start();
?>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">



    <form id="contact-form" method="post" action="store.php" role="form">

        <div class="messages"></div>
        <h1>ADD NEW</h1>
        <div class="controls">
            <div class="row">

                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="product_title">Title</label>
                        <input id="product_title"
                               value=""
                               type="text"
                               name="product_title"
                               placeholder="e.g.samsung" class="form-control"
                               autofocus="autofocus";>

                        <div class="help-block text-muted">Enter cart</div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="sid">Sid</label>
                        <input id="sid"
                               value=""
                               type="text"
                               name="sid"
                               placeholder="" class="form-control"
                               autofocus="autofocus";>

                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="product_id">product_id</label>
                        <input id="product_id"
                               value=""
                               type="text"
                               name="product_id
                               placeholder="" class="form-control"
                               autofocus="autofocus";>

                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="picture">picture</label>
                        <input id="picture"
                               value=""
                               type="file"
                               name="picture"
                               placeholder="" class="form-control"
                               autofocus="autofocus";>


                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="qty">quantity</label>
                        <input id="qty"
                               value=""
                               type="text"
                               name="qty"
                               placeholder="" class="form-control"
                               autofocus="autofocus";>


                    </div>
                </div>


        <div class="col-lg-12">
            <div class="form-group">
                <label for="unit_price">Unit_pice:</label>
                <input id="unit_price"
                       value=""
                       type="text"
                       name="unit_price"
                       placeholder="" class="form-control"
                       autofocus="autofocus";>


            </div>
        </div>
        <div class="col-lg-12">
            <div class="form-group">
                <label for="total_price">total_pice:</label>
                <input id="total_price"
                       value=""
                       type="text"
                       name="total_price"
                       placeholder="" class="form-control"
                       autofocus="autofocus";>


            </div>
        </div>

        </div>
        </div>

             <button type="submit" class="btn btn-success">send & Save carts</a></button>

    </form>

</main>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>



