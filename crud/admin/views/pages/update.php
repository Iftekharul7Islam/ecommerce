
<?php
$id=$_POST['id'];
$title=$_POST['title'];
$description=$_POST['description'];
$link=$_POST['link'];
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');
$query="UPDATE `pages` SET `title` = :title,
 `link`=:link ,
 `description`=:description
 WHERE`pages`.`id` = :id;";
$sth = $conn->prepare($query);
$sth->bindparam(':id',$id);
$sth->bindparam(':title',$title);
$sth->bindparam(':link',$link);
$sth->bindparam(':description',$description);
$result=$sth->execute();
header("location:index.php");