<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');
?>

<?php
ob_start();
?>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">



    <form id="contact-form" method="post" action="store.php" role="form">

        <div class="messages"></div>
        <h1>ADD NEW</h1>
        <div class="controls">
            <div class="row">

                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input id="title"
                               value=""
                               type="text"
                               name="title"
                               placeholder="e.g.samsung" class="form-control"
                               autofocus="autofocus";>

                        <div class="help-block text-muted">Enter page Title</div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="description">Description</label>
                        <input id="description"
                               value=""
                               type="text"
                               name="description"
                               placeholder="" class="form-control"
                               autofocus="autofocus";>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="link">Link</label>
                        <input id="link"
                               value=""
                               type="text"
                               name="link"
                               placeholder="" class="form-control"
                               autofocus="autofocus";>
                    </div>
                </div>

            </div>
        </div>

        <button type="submit" class="btn btn-success">send & Save sponsers</a></button>

    </form>

</main>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>




