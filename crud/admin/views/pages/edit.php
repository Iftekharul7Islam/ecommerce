<?php
//connect to database
include_once($_SERVER['DOCUMENT_ROOT'].'/crud/bootstrap.php');

$query="select * from pages where id=".$_GET['id'];
$sth = $conn->prepare($query);
$sth->execute();
$pages=$sth->fetch(PDO::FETCH_ASSOC);
//print_r($sponser);
?>
<?php
ob_start();
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">



    <form id="contact-form" method="post" action="update.php" role="form">
        <input id="id"  value="<?php echo $pages['id']?>"
               type="hidden" name="id" class="form-control">
        <div class="messages"></div>
        <h1>Edit</h1>
        <div class="controls">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">

                    &nbsp;
                        &nbsp;
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input id="title"
                                   value="<?php echo $pages['title']?>"
                                   type="text"
                                   name="title"
                                   placeholder="e.g.samsung" class="form-control"
                                   autofocus="autofocus";>

                            <div class="help-block text-muted">Enter page Title</div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="link">Link</label>
                            <input id="link"
                                   value="<?php echo $pages['link']?>"
                                   type="text"
                                   name="link"
                                   placeholder="" class="form-control"
                                   autofocus="autofocus";>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="des">Description</label>
                            <input id="des"
                                   value="<?php echo $pages['description']?>"
                                   type="text"
                                   name="des"
                                   placeholder="" class="form-control"
                                   autofocus="autofocus";>
                        </div>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-success">send & Save</a></button>

        </div>

    </form>
</main>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace('##MAIN_CONTENT##', $pagecontent, $layout);
?>
