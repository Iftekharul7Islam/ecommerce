<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link active" href="#">
                    <span data-feather="home"></span>
                    Dashboard <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=VIEW;?>orders/index.php">
                    <span data-feather="file"></span>
                    Orders
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=VIEW;?>admin/index.php">
                    <span data-feather="users"></span>
                    Admins
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=VIEW;?>subscribers/index.php">
                    <span data-feather="users"></span>
                    Subscribers
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=VIEW;?>product/index.php">
                    <span data-feather="shopping-cart"></span>
                    Products
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=VIEW;?>categories/index.php">
                    <span data-feather="circle"></span>
                    Categories
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=VIEW;?>brand/index.php">
                    <span data-feather="circle"></span>
                    Brands
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=VIEW;?>label/index.php">
                    <span data-feather="circle"></span>
                    Labels
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=VIEW;?>banner/index.php">
                    <span data-feather="circle"></span>
                    Banners
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=VIEW;?>contact/index.php">
                    <span data-feather="circle"></span>
                    Contacts
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=VIEW;?>carts/index.php">
                    <span data-feather="circle"></span>
                    Carts
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=VIEW;?>pages/index.php">
                    <span data-feather="circle"></span>
                    Pages
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=VIEW;?>sponsers/index.php">
                    <span data-feather="circle"></span>
                    Sponsers
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=VIEW;?>tags/index.php">
                    <span data-feather="circle"></span>
                    Tags
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=VIEW;?>popular_tag/index.php">
                    <span data-feather="circle"></span>
                    Popular Tags
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=VIEW;?>testimonial/index.php">
                    <span data-feather="circle"></span>
                    Testimonial
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span data-feather="users"></span>
                    Customers
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span data-feather="bar-chart-2"></span>
                    Reports
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span data-feather="layers"></span>
                    Integrations
                </a>
            </li>
        </ul>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>Saved reports</span>
            <a class="d-flex align-items-center text-muted" href="#">
                <span data-feather="plus-circle"></span>
            </a>
        </h6>
        <ul class="nav flex-column mb-2">
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span data-feather="file-text"></span>
                    Current month
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span data-feather="file-text"></span>
                    Last quarter
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span data-feather="file-text"></span>
                    Social engagement
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span data-feather="file-text"></span>
                    Year-end sale
                </a>
            </li>
        </ul>
    </div>
</nav>